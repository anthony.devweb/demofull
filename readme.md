# Demo 

Demo est un site rassemblant plusieurs technologie afin de faire un exercice de mise en application

## Environnemt de développement 

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante ( de la CLI symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement 

```bash
docker-compose up -d 
symfony serve -d
```

### Lancer les tests unitaires

```bash
php bin/phpunit --testdox
```