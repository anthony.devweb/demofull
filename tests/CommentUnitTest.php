<?php

namespace App\Tests;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentUnitTest extends TestCase
{
    public function testIsEqual()
    {
        $user = new User();

        $user->setEmail('test@test.fr')
            ->setPseudo('username')
            ->setPassword('password')
            ->setAvatar('testAvatar');

        $post = new Post();

        $post->setMessage('message')
            ->setVideo('video')
            ->setPicture('picture')
            ->setSlug('slug');

        $comment = new Comment();

        $date = new DateTime('NOW');

        $comment->setMessage('message')
            ->setVideo('video')
            ->setImage('picture')
            ->setDate($date)
            ->setUser($user)
            ->setPost($post);

        $this->assertEquals($comment->getMessage(), 'message');
        $this->assertEquals($comment->getVideo(), 'video');
        $this->assertEquals($comment->getImage(), 'picture');
        $this->assertEquals($comment->getDate(), $date);
        $this->assertEquals($comment->getUser(), $user);
        $this->assertEquals($comment->getPost(), $post);
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('test@test.fr')
            ->setPseudo('username')
            ->setPassword('password')
            ->setAvatar('testAvatar');

        $post = new Post();

        $post->setMessage('message')
            ->setVideo('video')
            ->setPicture('picture')
            ->setSlug('slug')
            ->setUser($user);

        $comment = new Comment();

        $date = new DateTime('NOW');

        $comment->setMessage('message')
            ->setVideo('video')
            ->setImage('picture')
            ->setDate($date)
            ->setUser($user)
            ->setPost($post);

        $fakeUser = [];
        $fakePost = [];

        $this->assertFalse($comment->getMessage() === 'false');
        $this->assertFalse($comment->getVideo() === 'false');
        $this->assertFalse($comment->getImage() === 'false');
        $this->assertFalse($comment->getDate() === 'false');
        $this->assertFalse($comment->getUser() === $fakeUser);
        $this->assertFalse($comment->getPost() === $fakePost);
    }

    public function testIsEmpty()
    {
        $comment = new Comment();

        $this->assertEmpty($comment->getMessage());
        $this->assertEmpty($comment->getVideo());
        $this->assertEmpty($comment->getImage());
        $this->assertEmpty($comment->getDate());
        $this->assertEmpty($comment->getUser());
        $this->assertEmpty($comment->getUser());
        $this->assertEmpty($comment->getPost());
    }
}
