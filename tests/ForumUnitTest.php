<?php

namespace App\Tests;

use App\Entity\Forum;
use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class ForumUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $category = new Category();

        $category->setName('name');

        $forum = new Forum();

        $forum->setName('name')
            ->setImage('image')
            ->setSlug('slug')
            ->setCategory($category);

        $this->assertTrue($forum->getName() === 'name');
        $this->assertTrue($forum->getImage() === 'image');
        $this->assertTrue($forum->getSlug() === 'slug');
        $this->assertTrue($forum->getCategory() === $category);
    }

    public function testIsFalse()
    {
        $category = new Category();

        $category->setName('name');

        $forum = new Forum();

        $forum->setName('name')
            ->setImage('image')
            ->setSlug('slug')
            ->setCategory($category);

        $this->assertFalse($forum->getName() === 'false');
        $this->assertFalse($forum->getImage() === 'false');
        $this->assertFalse($forum->getSlug() === 'false');
        $this->assertFalse($forum->getCategory() === 'false');
    }

    public function testIsEmpty()
    {
        $forum = new Forum();

        $this->assertEmpty($forum->getName());
        $this->assertEmpty($forum->getImage());
        $this->assertEmpty($forum->getSlug());
        $this->assertEmpty($forum->getCategory());
    }
}
