<?php

namespace App\Tests;

use App\Entity\Forum;
use App\Entity\Post;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertTrue;

class PostUnitTest extends TestCase
{
    public function testIsEqual()
    {
        $user = new User();

        $user->setEmail('test@test.fr')
            ->setPseudo('username')
            ->setPassword('password')
            ->setAvatar('testAvatar');

        $forum = new Forum();

        $forum->setName('name')
            ->setImage('image')
            ->setSlug('slug');

        $date = new DateTime('NOW');

        $post = new Post();

        $post->setMessage('message')
            ->setVideo('video')
            ->setPicture('picture')
            ->setSlug('slug')
            ->setUser($user)
            ->setDate($date)
            ->setForum($forum);

        $this->assertEquals($post->getMessage(), 'message');
        $this->assertEquals($post->getVideo(), 'video');
        $this->assertEquals($post->getPicture(), 'picture');
        $this->assertEquals($post->getSlug(), 'slug');
        $this->assertEquals($post->getDate(), $date);
        $this->assertEquals($post->getUser(), $user);
        $this->assertEquals($post->getForum(), $forum);
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('test@test.fr')
            ->setPseudo('username')
            ->setPassword('password')
            ->setAvatar('testAvatar');

        $forum = new Forum();

        $forum->setName('name')
            ->setImage('image')
            ->setSlug('slug');

        $date = new DateTime('NOW');

        $post = new Post();

        $post->setMessage('message')
            ->setVideo('video')
            ->setPicture('picture')
            ->setSlug('slug')
            ->setDate($date)
            ->setUser($user);
        $fakeUser = [];
        $fakeForum = [];

        $this->assertFalse($post->getMessage() === 'false');
        $this->assertFalse($post->getVideo() === 'false');
        $this->assertFalse($post->getPicture() === 'false');
        $this->assertFalse($post->getSlug() === 'false');
        $this->assertFalse($post->getDate() === '');
        $this->assertFalse($post->getUser() === $fakeUser);
        $this->assertFalse($post->getForum() === $fakeForum);
    }

    public function testIsEmpty()
    {
        $post = new Post();

        $this->assertEmpty($post->getMessage());
        $this->assertEmpty($post->getVideo());
        $this->assertEmpty($post->getPicture());
        $this->assertEmpty($post->getSlug());
        $this->assertEmpty($post->getDate());
        $this->assertEmpty($post->getUser());
        $this->assertEmpty($post->getForum());
    }
}
