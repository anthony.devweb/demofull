<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail('test@test.fr')
            ->setPseudo('username')
            ->setPassword('password')
            ->setAvatar('testAvatar');

        $this->assertTrue($user->getUserIdentifier() === 'test@test.fr');
        $this->assertTrue($user->getPseudo() === 'username');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAvatar() === 'testAvatar');
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('test@test.fr')
            ->setPseudo('username')
            ->setPassword('password')
            ->setAvatar('testAvatar');

        $this->assertFalse($user->getUserIdentifier() === 'false@test.fr');
        $this->assertFalse($user->getPseudo() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAvatar() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getUserIdentifier());
        $this->assertEmpty($user->getPseudo());
        $this->assertEmpty($user->getAvatar());
    }
}
